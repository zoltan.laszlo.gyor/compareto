package main;

public class Main {
	public static void main(String[] args) {
		String text1 = "This is an example";
		String text2 = "This is an example";
		String text3 = "I have no idea what is this?";
		
		System.out.println("The answer is: "+ text1.compareTo(text2)); //beacuse they are equal-> 0
		System.out.println("The answer is: "+ text1.compareTo(text3)); 
		System.out.println("The answer is: "+ text3.compareTo(text1));
	}
}
